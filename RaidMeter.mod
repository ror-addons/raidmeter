<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="RaidMeter" version="1.0" date="16/5/2020">
		<Author name="Sullemunk, Dalen" />
		<Description text="Displays the current list of Stats in Instances, made by Sullemunk(Client) and Dalen(Server)" />
		<VersionSettings gameVersion="1.4.8" windowsVersion="1.0" savedVariablesVersion="1.0" />
		<WARInfo>
			<Categories>
		        <Category name="RVR" />
		        <Category name="COMBAT" />
				</Categories>
			<Careers>
				<Career name="BLACKGUARD" />
				<Career name="WITCH_ELF" />
				<Career name="DISCIPLE" />
				<Career name="SORCERER" />
				<Career name="IRON_BREAKER" />
				<Career name="SLAYER" />
				<Career name="RUNE_PRIEST" />
				<Career name="ENGINEER" />
				<Career name="BLACK_ORC" />
				<Career name="CHOPPA" />
				<Career name="SHAMAN" />
				<Career name="SQUIG_HERDER" />
				<Career name="WITCH_HUNTER" />
				<Career name="KNIGHT" />
				<Career name="BRIGHT_WIZARD" />
				<Career name="WARRIOR_PRIEST" />
				<Career name="CHOSEN" />
				<Career name="MARAUDER" />
				<Career name="ZEALOT" />
				<Career name="MAGUS" />
				<Career name="SWORDMASTER" />
				<Career name="SHADOW_WARRIOR" />
				<Career name="WHITE_LION" />
				<Career name="ARCHMAGE" />
			</Careers>
		</WARInfo>
		<Dependencies>
            <Dependency name="EATemplate_Icons" />				
            <Dependency name="EASystem_WindowUtils" />			
			<Dependency name="EA_ChatWindow" />		
			<Dependency name="EASystem_Utils" />
            <Dependency name="EASystem_WindowUtils" />
            <Dependency name="EA_LegacyTemplates" />
            <Dependency name="EASystem_Tooltips" />            
            <Dependency name="EASystem_LayoutEditor" />
            <Dependency name="LibSlash" />			
		</Dependencies>
		<Files>
			<File name="RaidMeter.lua" />

			<File name="RaidMeter.xml" />
		</Files>
		<SavedVariables>
			</SavedVariables>
		<OnInitialize>
			<CallFunction name="RaidMeter.Initialize" />				
		</OnInitialize>
        <OnShutdown>
			<CallFunction name="RaidMeter.Shutdown" />
		</OnShutdown> 		
		<OnUpdate>
		</OnUpdate>
	</UiMod>
</ModuleFile>