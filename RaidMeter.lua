RaidMeter = {}
local Version = "1.0"

function RaidMeter.Initialize()
	RegisterEventHandler(TextLogGetUpdateEventId("Chat"), "RaidMeter.OnChatLogUpdated") -- runs the function when Chatlog gets a new text
	if LibSlash then LibSlash.RegisterSlashCmd("RaidMeter", function() RaidMeter.Close() end) end
	
	RaidMeter.Enabled = true
	RaidMeter.Debug = true
	RaidMeter.List = {}
	RaidMeter.TotalDamage = 0
	RaidMeter.TotalMitigated = 0
	RaidMeter.TotalHealed = 0
	RaidMeter.SelectedType = 3
	RaidMeter.SelectedTab = 1
	RaidMeter.Texts = {L"Damage Done", L"Healing Done", L"Damage Taken", L"Mitigation"}	

		CreateWindow("RaidMeterWindow", false)
			for i=1,6 do	
				if DoesWindowExist("RaidMeterWindow_RaidWindow"..i) == false then
				CreateWindowFromTemplate("RaidMeterWindow_RaidWindow"..i, "Template_RaidWindow", "RaidMeterWindow")

				WindowClearAnchors("RaidMeterWindow_RaidWindow"..i)
				WindowAddAnchor("RaidMeterWindow_RaidWindow"..i,"topleft", "RaidMeterWindow","topleft", 0,10+(32*i))
				--WindowSetOffsetFromParent("RaidMeterWindow_RaidWindow"..i,0,32*(i))
			end	
				--local LabelW,LabelH = WindowGetDimensions("RaidMeterWindow_RaidWindow"..i.."TimerBarText")			
				--WindowSetDimensions("RaidMeterWindow_RaidWindow"..i.."TimerBarText",100,LabelH)					
				StatusBarSetMaximumValue("RaidMeterWindow_RaidWindow"..i.."TimerBar", 100  )
				StatusBarSetForegroundTint( "RaidMeterWindow_RaidWindow"..i.."TimerBar", DefaultColor.GREEN.r, DefaultColor.GREEN.g, DefaultColor.GREEN.b )
				StatusBarSetBackgroundTint( "RaidMeterWindow_RaidWindow"..i.."TimerBar", DefaultColor.BLACK.r, DefaultColor.BLACK.g, DefaultColor.BLACK.b )	
				--LabelSetText("RaidMeterWindow_RaidWindow"..i.."TimerBarText",L"")					
				LabelSetText("RaidMeterWindow_RaidWindow"..i.."Label",L"")
				WindowSetShowing("RaidMeterWindow_RaidWindow"..i,false)	
			end	

	for i=1,4 do
		ComboBoxAddMenuItem("RaidMeterWindowCombo",towstring(RaidMeter.Texts[i]))
	end

	ComboBoxSetSelectedMenuItem("RaidMeterWindowCombo",1)
	
	
	    WindowRegisterEventHandler( "RaidMeterWindow", SystemData.Events.TOGGLE_SCENARIO_SUMMARY_WINDOW,  "RaidMeter.ToggleShowing" )
	
		ButtonSetText("RaidMeterWindowTotalTab", L"Total")
		ButtonSetText("RaidMeterWindowBossTab", L"Boss")
		ButtonSetPressedFlag( "RaidMeterWindowTotalTab",true)		
		ButtonSetPressedFlag( "RaidMeterWindowBossTab",false)		
	
end

function RaidMeter.Shutdown()
	UnregisterEventHandler(TextLogGetUpdateEventId("Chat"), "RaidMeter.OnChatLogUpdated")	
end

function RaidMeter.OnChatLogUpdated(updateType, filterType)
	if RaidMeter.Enabled == true then
		if( updateType == SystemData.TextLogUpdate.ADDED ) then 		
			if filterType == SystemData.ChatLogFilters.CHANNEL_9 then	
				local _, filterId, text = TextLogGetEntry( "Chat", TextLogGetNumEntries("Chat") - 1 ) 
				if text:find(L"SCINSTANCE") then 
					RaidMeter.SplitText(text)
				end
			end
		end
	end		
end

function RaidMeter.SplitText(text)
	if text ~= nil then
		text = tostring(text)
	end
	
	RaidMeter.List = {}
	RaidMeter.TotalDamage = 0
	RaidMeter.CurrentStat = {}
	RaidMeter.TotalStats = {[0]=0,[1]=0,[2]=0,[3]=0,[4]=0,[5]=0,[6]=0,[7]=0,[8]=0,[9]=0,[10]=0,[11]=0}
	xListSplit = StringSplit(text, ";")
	if xListSplit[2] ~= "" then
		for k,v in pairs(xListSplit) do	
			if k ~= 1 then
				xStatSplit = StringSplit(v, ",")
				RaidMeter.List[k-1] = xStatSplit				
				RaidMeter.CurrentStat[k-1] =  xStatSplit[RaidMeter.SelectedType]
				RaidMeter.TotalDamage = RaidMeter.TotalDamage + (xStatSplit[RaidMeter.SelectedType] or 0)
				WindowSetShowing("RaidMeterWindow_RaidWindow"..(k-1),true)					
				for i=3,10 do			
					RaidMeter.TotalStats[i] = tonumber(RaidMeter.TotalStats[i]) + tonumber(xStatSplit[i]) or 0
				end
			end
		end	
	end
	WindowSetDimensions("RaidMeterWindow",410,52+(30*(#xListSplit-1)))	
	RaidMeter.UpdateStuff()	
end

--Name, CareerId, TotalDamage, TotalDamage_Since_Boss, TotalHealing, TotalHealing_Since_Boss, TotalDamageTaken, TotalDamageTaken_Since_Boss, TotalMitigated, TotalMitigated_Since_Boss

function RaidMeter.UpdateStuff()

	for i=1,6 do
		WindowSetShowing("RaidMeterWindow_RaidWindow"..i,false)
	end
	
	table.sort( RaidMeter.List,RaidMeter.SortCompare )

	for k,v in ipairs(RaidMeter.List) do	
		if RaidMeter.List[k] ~= nil then
			WindowSetShowing("RaidMeterWindow_RaidWindow"..k,true)
			
		local percent = RaidMeter.List[k][RaidMeter.SelectedType]/RaidMeter.TotalStats[RaidMeter.SelectedType]
		local tint = math.min(percent, 0.5) * 2
		StatusBarSetForegroundTint( "RaidMeterWindow_RaidWindow"..k.."TimerBar", 255*tint, 255*(1-tint), 0)
		StatusBarSetCurrentValue("RaidMeterWindow_RaidWindow"..k.."TimerBar", percent*100)			
						
		--	StatusBarSetForegroundTint( "RaidMeterWindow_RaidWindow"..k.."TimerBar", 255*(((RaidMeter.List[k][RaidMeter.SelectedType])/(RaidMeter.TotalStats[RaidMeter.SelectedType])*100 )/50), 155*(1-(((RaidMeter.List[k][RaidMeter.SelectedType])/(RaidMeter.TotalStats[RaidMeter.SelectedType])*100 )/100)), 0)
		--	StatusBarSetCurrentValue("RaidMeterWindow_RaidWindow"..k.."TimerBar", ((RaidMeter.List[k][RaidMeter.SelectedType])/(RaidMeter.TotalStats[RaidMeter.SelectedType])*100 ))

			local SubZero = ((RaidMeter.List[k][RaidMeter.SelectedType])/(RaidMeter.TotalStats[RaidMeter.SelectedType])*100 )
			if (SubZero >= 0) == false then SubZero = 0.0 end
						
			LabelSetText("RaidMeterWindow_RaidWindow"..k.."Label",towstring(RaidMeter.List[k][1]))	
			LabelSetText("RaidMeterWindow_RaidWindow"..k.."Value",towstring(RaidMeter.List[k][RaidMeter.SelectedType])..L" ("..wstring.format(L"%.01f",SubZero)..L"%)")	

			local txtr, x, y, disabledTexture = GetIconData (Icons.GetCareerIconIDFromCareerLine(tonumber(RaidMeter.List[k][2])))	
			CircleImageSetTexture("RaidMeterWindow_RaidWindow"..k.."ButtonIcon",txtr, 16, 16)
		end
	end
end


function RaidMeter.SortCompare( index1, index2 )
    if( index2[RaidMeter.SelectedType] == nil ) then
        return false
    end
    
    if( index1[RaidMeter.SelectedType] == nil ) then
        return false
    end
    
    if( index1[RaidMeter.SelectedType] == nil or index2[RaidMeter.SelectedType] == nil ) then
        return false
    end

	return (tonumber(index1[RaidMeter.SelectedType]) > tonumber(index2[RaidMeter.SelectedType]))
end

function RaidMeter.MenuSelect(idx)
	RaidMeter.SelectedType = (2*idx)+RaidMeter.SelectedTab
	PlaySound(313)	
	RaidMeter.UpdateStuff()
end

function RaidMeter.ToggleShowing()
	if  ( not GameData.Player.isInScenario) and (not GameData.Player.isInSiege) and ( not WindowGetShowing("ScenarioSummaryWindow")) then
		WindowSetShowing("RaidMeterWindow",not WindowGetShowing("RaidMeterWindow"))
    end
end

function RaidMeter.Close()
  WindowSetShowing("RaidMeterWindow",not WindowGetShowing("RaidMeterWindow"))  
end

function RaidMeter.OnTabLBU()
	local tabNumber	= WindowGetId (SystemData.ActiveWindow.name)
	RaidMeter.SelectedTab = tabNumber
	ButtonSetPressedFlag( "RaidMeterWindowTotalTab",tabNumber==1)		
	ButtonSetPressedFlag( "RaidMeterWindowBossTab",tabNumber==2)	
	local SelectedMenu = ComboBoxGetSelectedMenuItem("RaidMeterWindowCombo")
	RaidMeter.SelectedType = (2*SelectedMenu)+RaidMeter.SelectedTab
	RaidMeter.UpdateStuff()
end